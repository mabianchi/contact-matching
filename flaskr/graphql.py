import uuid

import graphene

from .db import get_db
from .sql import (delete_contact, delete_user, insert_user,
                  select_all_contactdata_by_contact_uuid,
                  select_contact_by_uuid, select_resolve_match_contact,
                  select_user_by_name)


# Methods
def create_user(username):
    user_uuid = uuid.uuid4()
    row_id = insert_user(username, user_uuid)
    return User(**{'id': row_id, 'name': username, 'uuid': user_uuid})


def create_contact(name, user_id, user_uuid):
    contact_uuid = str(uuid.uuid4())
    db = get_db()
    cursor = db.execute(
        'INSERT INTO contact (name, uuid, user_id) VALUES (?, ?, ?)',
        (name, contact_uuid, user_id)
    )
    db.commit()
    return Contact(**{
        'id': cursor.lastrowid,
        'uuid': contact_uuid,
        'user_id': user_uuid,
        'name': name,
        'contact_data': [],
    })


def create_contact_data(contact_id, contact_type, value):
    db = get_db()
    db.execute(
        'INSERT INTO contact_data (contact_id, type, value) VALUES (?, ?, ?)',
        (contact_id, contact_type, value)
    )
    db.commit()
    return ContactData(type=contact_type, value=value)


def add_contact(user_uuid, contact_name, contact_info):
    """
    contact_info: List({type, value})
    """
    db = get_db()

    # Get user
    row = db.execute(
        'SELECT * FROM user WHERE uuid=?',
        (user_uuid, )
    ).fetchone()
    user = User(**dict(zip(row.keys(), tuple(row))))

    # Create contact
    contact = create_contact(contact_name, user.id, user_uuid)

    all_contact_data = []
    for raw_contact_data in contact_info:
        # Create contact data
        contact_data = create_contact_data(
            contact.id,
            raw_contact_data['type'],
            raw_contact_data['value']
        )
        all_contact_data.append(contact_data)

    contact.contact_data = all_contact_data

    return contact


def get_contact_by_id(contact_id):
    db = get_db()
    contact_data_rows = db.execute(
        'SELECT type, value FROM contact_data WHERE contact_id=?',
        (contact_id, )
    ).fetchall()
    contact_data = [
        ContactData(**dict(zip(row.keys(), tuple(row))))
        for row in contact_data_rows
    ]
    contact_row = db.execute(
        '''
        SELECT c.id, c.uuid, c.name, u.uuid as user_id
        FROM contact as c
        LEFT JOIN user as u on u.id = c.user_id
        WHERE c.id=?
        ''',
        (contact_id, )
    ).fetchone()
    contact = Contact(
        **dict(zip(contact_row.keys(), tuple(contact_row))))
    contact.contact_data = contact_data
    return contact


class ContactData(graphene.ObjectType):
    type = graphene.String()
    value = graphene.String()


class Contact(graphene.ObjectType):
    id = graphene.Int()
    uuid = graphene.String()
    user_id = graphene.String(required=False)
    contact_id = graphene.String()
    name = graphene.String()
    contact_data = graphene.List(ContactData)

    def resolve_contact_id(root, info):
        return root.uuid


class User(graphene.ObjectType):
    id = graphene.Int()
    uuid = graphene.String()
    user_id = graphene.String()
    name = graphene.String()

    def resolve_user_id(root, info):
        return root.uuid


class CreateUser(graphene.Mutation):
    class Arguments:
        name = graphene.String()

    user_id = graphene.String()
    name = graphene.String()

    def mutate(parent, info, name):
        user = create_user(name)
        return CreateUser(user_id=user.uuid, name=user.name)


class RemoveUser(graphene.Mutation):
    class Arguments:
        user_id = graphene.String(required=True)

    ok = graphene.Boolean(required=False)

    def mutate(parent, info, user_id):
        delete_user(user_id)
        return RemoveUser(ok=True)


class RemoveContact(graphene.Mutation):
    class Arguments:
        contact_id = graphene.String(required=True)

    ok = graphene.Boolean(required=False)

    def mutate(parent, info, contact_id):
        delete_contact(contact_id)
        return RemoveContact(ok=True)


class ContactDataInput(graphene.InputObjectType):
    type = graphene.String()
    value = graphene.String()


class UserAndContacts(graphene.InputObjectType):
    user_id = graphene.String(required=True)
    name = graphene.String(required=True)
    contact_data = graphene.List(ContactDataInput)


class ContactDataListInput(graphene.InputObjectType):
    contact_data = graphene.List(ContactDataInput)


class AddContacts(graphene.Mutation):
    class Arguments:
        input = graphene.List(UserAndContacts)

    contacts = graphene.List(Contact)

    def mutate(parent, info, input):
        contacts = []
        for contact_info in input:
            user_id = contact_info.pop('user_id')
            contact_name = contact_info.pop('name')
            contact = add_contact(
                user_id, contact_name, contact_info['contact_data'])
            contacts.append(contact)
        return AddContacts(contacts=contacts)


class Query(graphene.ObjectType):
    get_user = graphene.Field(User, name=graphene.String(required=True))
    get_contact = graphene.Field(Contact, contact_id=graphene.String(required=True))
    match_contact = graphene.Field(Contact, input=ContactDataListInput())

    def resolve_get_user(root, info, name):
        row = select_user_by_name(name)
        return User(**dict(zip(row.keys(), tuple(row))))

    def resolve_get_contact(root, info, contact_id):
        row = select_contact_by_uuid(contact_id)
        contact_data_rows = select_all_contactdata_by_contact_uuid(contact_id)
        contact_data = [
            ContactData(**dict(zip(r.keys(), tuple(r))))
            for r in contact_data_rows
        ]
        user_id = row['user_id']
        contact_pk = row['contact_pk']
        contact_name = row['contact_name']
        contact = Contact(**{
            'id': contact_pk,
            'uuid': contact_id,
            'user_id': user_id,
            'contact_id': contact_id,
            'name': contact_name,
            'contact_data': contact_data,
        })
        return contact

    def resolve_match_contact(root, info, input):
        contact_id = select_resolve_match_contact(input['contact_data'])

        contact = Contact()
        if contact_id:
            # Take first matching contact
            contact = get_contact_by_id(contact_id)

        return contact


class Mutation(graphene.ObjectType):
    add_user = CreateUser.Field()
    remove_user = RemoveUser.Field()
    add_contacts = AddContacts.Field()
    remove_contact = RemoveContact.Field()
