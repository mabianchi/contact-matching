import json

import graphene
from flask import Blueprint, request

from .graphql import Query, Mutation


bp = Blueprint('user', __name__, url_prefix='/users')


@bp.route('/graphql/', methods=['POST'])
def graphql_view():
    schema = graphene.Schema(query=Query, mutation=Mutation)
    result = schema.execute(request.data.decode('utf-8'))
    return json.dumps(result.to_dict())
