from .db import get_db


def insert_user(name, uuid):
    db = get_db()
    cursor = db.execute(
        'INSERT INTO user (name, uuid) VALUES (?, ?)',
        (name, str(uuid))
    )
    db.commit()
    return cursor.lastrowid


def select_all_users():
    db = get_db()
    return db.execute('SELECT * FROM user').fetchall()


def select_user_by_name(name):
    db = get_db()
    row = db.execute(
        'SELECT * FROM user WHERE name=?',
        (name, )
    ).fetchone()
    return row


def select_contact_by_uuid(uuid):
    db = get_db()
    row = db.execute(
        '''
        SELECT u.uuid AS user_id,
                c.id as contact_pk,
                c.name as contact_name
        FROM contact as c
        LEFT JOIN user as u ON c.user_id = u.id
        WHERE c.uuid=?;
        ''',
        (uuid,)
    ).fetchone()
    return row


def select_all_contactdata_by_contact_uuid(uuid):
    db = get_db()
    return db.execute(
        '''
        SELECT type, value
        FROM contact as c
        LEFT JOIN contact_data as cd ON c.id = cd.contact_id
        WHERE c.uuid=?;
        ''',
        (uuid,)
    ).fetchall()


def select_resolve_match_contact(contact_data):
    """
    contact_data: [{type, value}]

    returns:
        contact_id: int


    Search contacts data that matches with any of the contact data
    supplied. The where clause is a concatenation of OR clauses and each
    of those clauses is an AND. So we get all contact_data entries that
    matches with any of the "type" and "value" recieved.

    To get only contacts that have all searched contacts data, I group
    them by contact id and filter those who appears as many times as
    contacts data information was used to filter.

    For instance:
    Case 1:
        If filtered using an email only, this query will return
        contacts who have that email in its contact_data. If there are
        more than one contact, any of them will be returned.
    Case 2:
        If filtered using an email and a phone, this query will return
        contacts who have both, email and phone, in its contact_data.
        Again, if there are more than one contact that matches, any of them
        will be returned. A contact that has only the email or only the
        phone (but not both) in their contact_data won't be returned.
    """
    db = get_db()
    search_values_count = len(contact_data)
    where_clause = ' OR '.join(
        ["(cd.type=? AND cd.value=?)" for _ in range(search_values_count)]
    )
    sql = f'''
        SELECT c.id as contact_id
        FROM contact as c
        LEFT JOIN contact_data as cd ON c.id = cd.contact_id
        WHERE {where_clause}
        GROUP BY c.id
        HAVING COUNT(*)=?
    '''
    sql_params = [d[k] for d in contact_data for k in d]
    sql_params.append(search_values_count)
    rows = db.execute(sql, sql_params).fetchall()
    if rows:
        return rows[0]['contact_id']
    return None


def delete_user(user_uuid):
    db = get_db()
    db.execute(
        'DELETE FROM user WHERE uuid=?',
        (user_uuid, )
    )
    db.commit()


def delete_contact(contact_uuid):
    db = get_db()
    db.execute(
        'DELETE FROM contact WHERE uuid=?',
        (contact_uuid, )
    )
    db.commit()
