# Project description

https://www.notion.so/Backend-Task-Contact-Matching-fc20db4f623c4987b8c00e6699273813


# Development set up

* Download repo and open it in a terminal
* Create a python environment: `python3 -venv venv`
* Enter environment: `source venv/bin/activate`
* Initialize database: `FLASK_APP=flaskr flash init-db`
* Run development server: `./run.sh`


# Queries examples

All examples here assume you have curl installed.

## Add user
```bash
curl -X POST \
     -H "Content-Type: application/json" \
     -d 'mutation {
            addUser(name:"<username>") {
               userId
               name
            }
       }' \
     http://localhost:5000/users/graphql/
```

## Remove user
```bash
curl -X POST \
     -H "Content-Type: application/json" \
     -d 'mutation {
          removeUser(userId:"<UUID>") {
            ok
          }
       }' \
     http://localhost:5000/users/graphql/
```

## Get user
```bash
curl -X POST \
     -H "Content-Type: application/json" \
     -d 'query {
          getUser(name:"<some user name>") {
            name
            userId
          }
        }' \
     http://localhost:5000/users/graphql/
```



## Add contacts
```bash
curl -X POST \
     -H "Content-Type: application/json" \
     -d 'mutation {
          addContacts(input: [
            {
              userId: "<UUID>",
              name: "<contact_name>",
              contactData: [
                  {
                    type: "email",
                    value: "some@email.com"
                  }
              ]
              },
              {
              userId: "<UUID>",
              name: "<contact_name>",
              contactData: [
                  {
                    type: "email",
                    value: "farmadegre@gmail.com"
                  },
                  {
                    type: "phone",
                    value: "02323427909"
                  }
              ]
              }
          ])
          {
              contacts {
                      contactId
                      userId
                      name
                      contactData {
                          type
                          value
                      }
              }
          }
      }' \
     http://localhost:5000/users/graphql/
```

## Remove contact
```bash
curl -X POST \
     -H "Content-Type: application/json" \
     -d 'mutation {
          removeContact(contactId:"<UUID>") {
            ok
          }
       }' \
     http://localhost:5000/users/graphql/
```

## Get contact
```bash
curl -X POST \
     -H "Content-Type: application/json" \
     -d 'query {
          getContact(contactId:"<UUID>") {
            contactId
            userId
            contactData {
              type
              value
            }
          }
        }' \
      http://localhost:5000/users/graphql/
```

## Match contact
```bash
curl -X POST \
     -H "Content-Type: application/json" \
     -d 'query {
	        matchContact(input: {
            contactData: [
              {type: "phone", value: "1234"},
              {type: "email", value: "someone@domain.com"}
            ]
          }) {
		        contactId
            userId
            name
            contactData {
                type
                value
            }
          }
        }' \
      http://localhost:5000/users/graphql/
```


# Some notes about specification and code differences

1. Mutations for remove user and contact have an empty sub selection in the specification. I understand that it is always mandatory to make a sub selection in graphql (as a client) and as a server you always have to return something (there is no null return in graphql). So in both cases I opt to return a boolean to know if the result was ok
1. The result of "addContacts" seems logic to be a list of contacts. Nevertheless in the specification seems to be a list of something called contact which is a list of all data from contact. I understand it as a mistake in the specification and I just returned a list of contacts created.
1. There are differences between the API description in the first paragraphs and the queries and mutations detailed after that. I created only those detailed queries and mutations. If it is needed I can add the other queries but I think I covered the most difficult ones.
