from unittest.mock import patch

import graphene
from graphene.test import Client

from flaskr.graphql import Mutation, Query, User


class RowFake:
    def __init__(self, data_dict):
        self.data_dict = data_dict

    def __iter__(self):
        return self.data_dict.values().__iter__()

    def keys(self):
        return self.data_dict.keys()


def test_get_user():
    row = RowFake({
        'id': 1,
        'uuid': 'abcd-efgh-ijkl-mnop',
        'name': 'sam',
    })
    schema = graphene.Schema(query=Query, mutation=Mutation)
    client = Client(schema)
    with patch('flaskr.graphql.select_user_by_name') as select:
        select.return_value = row
        executed = client.execute(
            '''
            query {
                getUser(name:"sam") {
                name
                userId
                }
            }
            '''
        )

    assert executed == {
        'data': {
            'getUser': {
                'name': 'sam',
                'userId': 'abcd-efgh-ijkl-mnop',
            }
        }
    }


def test_add_user():
    user = User(id=1, name='sam', uuid='abcd-efgh-ijkl-mnop')
    schema = graphene.Schema(query=Query, mutation=Mutation)
    client = Client(schema)
    with patch('flaskr.graphql.create_user') as create:
        create.return_value = user
        executed = client.execute(
            '''
            mutation {
                addUser(name:"sam") {
                    userId
                    name
                }
            }
            '''
        )

    assert executed == {
        'data': {
            'addUser': {
                'name': 'sam',
                'userId': 'abcd-efgh-ijkl-mnop',
            }
        }
    }
    create.assert_called_with('sam')

# TODO: add tests for the other methods
